package com.example.oanamaria.coffeedripper;

/**
 * Class AppDefinitions defines the default values for the app.
 *
 * @author Oana Maria Teodorescu
 * @version 1.0
 */
public abstract class AppDefinitions {
    public final static int MIN_COFFEE_VALUE = 10;
    public final static int MAX_COFFEE_VALUE = 30;
    public final static double DEFAULT_W_PER_C_RATIO = 16.4;
    public final static long TIMER_INTERVAL = 90000;
}
