package com.example.oanamaria.coffeedripper;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.Callable;

/**
 * Activity MainActivity
 *
 * @author Oana Maria Teodorescu
 * @version 1.3
 */
public class MainActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener, View.OnClickListener {
    private PowerManager manager;
    private PowerManager.WakeLock wakeLock;
    private SeekBar sbCoffee, sbWater;
    private TextView txtCoffeeVal, txtWaterVal;
    private CircularStopwatch cwTimer;
    private TextView txtTimerValue;

    private int coffee_progress_range;
    private int water_progress_range;
    private int minWaterValue = 750;
    private int maxWaterValue = 750;
    private int timer_progress_half;

    private ObjectAnimator textBlinkAnimator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        manager = (PowerManager) getSystemService(Context.POWER_SERVICE);

        sbCoffee = (SeekBar) findViewById(R.id.sbCoffee);
        txtCoffeeVal = (TextView) findViewById(R.id.txtCoffeeValue);
        sbWater = (SeekBar) findViewById(R.id.sbWater);
        txtWaterVal = (TextView) findViewById(R.id.txtWaterValue);

        coffee_progress_range = AppDefinitions.MAX_COFFEE_VALUE - AppDefinitions.MIN_COFFEE_VALUE;

        minWaterValue = (int) (AppDefinitions.MIN_COFFEE_VALUE * AppDefinitions.DEFAULT_W_PER_C_RATIO + 0.5);
        maxWaterValue = (int) (AppDefinitions.MAX_COFFEE_VALUE * AppDefinitions.DEFAULT_W_PER_C_RATIO + 0.5);
        water_progress_range = maxWaterValue - minWaterValue;

        txtCoffeeVal.setText(String.format("%3d", AppDefinitions.MIN_COFFEE_VALUE));
        txtWaterVal.setText(String.format("%3d", minWaterValue));

        sbCoffee.setMax(coffee_progress_range);
        sbWater.setMax(water_progress_range);

        sbCoffee.setOnSeekBarChangeListener(this);
        sbWater.setOnSeekBarChangeListener(this);

        cwTimer = (CircularStopwatch) findViewById(R.id.cwTimer);
        cwTimer.setOnClickListener(this);
        timer_progress_half = (int) (AppDefinitions.TIMER_INTERVAL / 1000);
        cwTimer.setMax(2 * timer_progress_half);

        txtTimerValue = (TextView) findViewById(R.id.txtTimerValue);
        txtTimerValue.setOnClickListener(this);

        wakeLock = manager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "com.example.oanamaria:CoffeeDripper");
        wakeLock.acquire();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser) {
            if (seekBar == sbCoffee) {
                int coffeeValue = progress + +AppDefinitions.MIN_COFFEE_VALUE;
                txtCoffeeVal.setText(String.format("%3d", coffeeValue));
                int val = (int) (coffeeValue * AppDefinitions.DEFAULT_W_PER_C_RATIO + 0.5);
                sbWater.setProgress(val - minWaterValue);
                txtWaterVal.setText(String.format("%3d", val));
            } else {
                int waterValue = progress + minWaterValue;
                txtWaterVal.setText(String.format("%3d", waterValue));
                int val = (int) (waterValue / AppDefinitions.DEFAULT_W_PER_C_RATIO + 0.5);
                sbCoffee.setProgress(val - AppDefinitions.MIN_COFFEE_VALUE);
                txtCoffeeVal.setText(String.format("%3d", val));
            }
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onClick(View v) {
        final int id = v.getId();
        switch (id) {
            case R.id.cwTimer:
            case R.id.txtTimerValue:
                if (cwTimer.isAnimationRunning()) {
                    cwTimer.cancelAnimation();

                    // Start blinking animation
                    cwTimer.setBlinkingAnimation(700, 3);
                    startTextBlinkingAnimation(txtTimerValue, "textColor",
                            getResources().getColor(R.color.darkCoffee), 700, 3);
                } else {
                    // End blink animation
                    cwTimer.setColor(getResources().getColor(R.color.coffee));
                    cwTimer.cancelBlink(getResources().getColor(R.color.coffee));
                    txtTimerValue.setTextColor(getResources().getColor(R.color.darkCoffee));
                    endTextBlinkingAnimation(getResources().getColor(R.color.darkCoffee));

                    cwTimer.setProgress(0);
                    cwTimer.setProgressWithAnimationAndListener(timer_progress_half, AppDefinitions.TIMER_INTERVAL,
                            new PostAnimatorListener(new Callable<Void>() {
                                @Override
                                public Void call() throws Exception {
                                    // Play audio sound
                                    AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                                    //int currentVolume = audio.getStreamVolume(AudioManager.STREAM_ALARM);
                                    ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 80);
                                    toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 300);

                                    // Message display
                                    Toast toast = Toast.makeText(getApplicationContext(), "Halfway there ...", Toast.LENGTH_LONG);
                                    toast.show();
                                    cwTimer.setProgressWithAnimationAndListener(2 * timer_progress_half,
                                            AppDefinitions.TIMER_INTERVAL,
                                            new PostAnimatorListener(new Callable<Void>() {
                                                @Override
                                                public Void call() throws Exception {
                                                    // Play audio sound
                                                    AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                                                    //int currentVolume = audio.getStreamVolume(AudioManager.STREAM_ALARM);
                                                    ToneGenerator toneG = new ToneGenerator(AudioManager.STREAM_ALARM, 90);
                                                    toneG.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD, 300);

                                                    // Message display
                                                    Toast toast = Toast.makeText(getApplicationContext(), "Enjoy! :D", Toast.LENGTH_LONG);
                                                    toast.show();

                                                    // Start final text blinking animation
                                                    startTextBlinkingAnimation(txtTimerValue, "textColor",
                                                            getResources().getColor(R.color.darkCoffee), 500, 1);

                                                    wakeLock.release();
                                                    return null;
                                                }
                                            }), new TimerAnimatorUpdateListener(AppDefinitions.TIMER_INTERVAL, txtTimerValue));
                                    return null;
                                }
                            }), new TimerAnimatorUpdateListener(0, txtTimerValue));
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
          /*
             * when the activity is resumed, we acquire a wake-lock so that the
	         * screen stays on, since the user will likely not be fiddling with the
	         * screen or buttons.
	         */

        try {
            wakeLock.acquire();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();

        // and release our wake-lock
        try {
            wakeLock.release();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Sets a blinking animation to the specified UI element.
     * <p>
     * <u>Note</u>: The repeat count must be an odd number for the UI element to be visible.
     *
     * @param target            The target UI element
     * @param colorProperty     The name of the color property to change during blinking
     * @param elementBlinkColor The color of the element during blinking
     * @param duration          The duration of animation in milliseconds
     * @param repeatCount       The number of times to repeat
     */
    private void startTextBlinkingAnimation(Object target, String colorProperty, int elementBlinkColor, long duration, int repeatCount) {
        textBlinkAnimator = ObjectAnimator.ofInt(target, colorProperty,
                elementBlinkColor, Color.TRANSPARENT);
        textBlinkAnimator.setDuration(duration);
        textBlinkAnimator.setEvaluator(new ArgbEvaluator());
        textBlinkAnimator.setRepeatCount(repeatCount);
        textBlinkAnimator.setRepeatMode(ValueAnimator.REVERSE);
        textBlinkAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animation) {
                txtTimerValue.setText("00:00");
                cwTimer.setProgress(0);
            }

            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        textBlinkAnimator.start();
    }

    private void endTextBlinkingAnimation(int initialElementColor) {
        if (textBlinkAnimator != null) {
            textBlinkAnimator.removeAllListeners();
            textBlinkAnimator.cancel();
            //ReflectionUtility.setFieldThroughSetter(textBlinkAnimator.getTarget(), textBlinkAnimator.getPropertyName(), initialElementColor);
        }
    }
}
