package com.example.oanamaria.coffeedripper;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Class ReflectionUtility
 *
 * @author Oana Maria Teodorescu
 * @version 1.0
 */
public class ReflectionUtility {
    @SuppressWarnings("unchecked")
    public static <E> E get(Object object, String fieldName) {
        Class<?> clazz = object.getClass();
        while (clazz != null) {
            try {
                Field field = clazz.getDeclaredField(fieldName);
                field.setAccessible(true);
                return (E) field.get(object);
            } catch (NoSuchFieldException e) {
                clazz = clazz.getSuperclass();
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
        return null;
    }

    public static boolean setField(Object object, String fieldName, Object fieldValue) {
        Class<?> clazz = object.getClass();
        while (clazz != null) {
            try {
                Field field = clazz.getDeclaredField(capitalizeFirstLetter(fieldName));
                field.setAccessible(true);
                field.set(object, fieldValue);
                return true;
            } catch (NoSuchFieldException e) {
                clazz = clazz.getSuperclass();
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
        return false;
    }

    public static boolean setFieldThroughSetter(Object object, String fieldName, Object fieldValue) {
        Class<?> clazz = object.getClass();
        while (clazz != null) {
            try {
                Method method = clazz.getMethod(ReflectionUtility.getSetterNameFromField(fieldName),
                        fieldValue.getClass());
                method.invoke(object, fieldValue);
                return true;
            } catch (NoSuchMethodException e) {
                clazz = clazz.getSuperclass();
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
        return false;
    }

    private static String capitalizeFirstLetter(String text) {
        return String.valueOf(text.charAt(0)).toUpperCase()
                + text.substring(1, text.length());
    }

    private static String getSetterNameFromField(String fieldName) {
        return "set" + capitalizeFirstLetter(fieldName);
    }
}
