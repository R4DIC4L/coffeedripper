package com.example.oanamaria.coffeedripper;

import android.animation.Animator;

import java.util.concurrent.Callable;

/**
 * Class PostAnimatorListener calls the passed post-animation function
 * after the animation has ended.
 *
 * @author Oana Maria Teodorescu
 * @version 1.1
 */
public class PostAnimatorListener implements Animator.AnimatorListener {
    private Callable<Void> endFunction;

    public PostAnimatorListener(Callable<Void> endFunction) {
        this.endFunction = endFunction;
    }

    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {
        try {
            endFunction.call();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }
}
