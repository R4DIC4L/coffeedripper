package com.example.oanamaria.coffeedripper;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.animation.DecelerateInterpolator;

import com.mrn.customprogressbar.CircleProgressBar;

/**
 * A subclass of {@link com.mrn.customprogressbar.CircleProgressBar} class
 * for creating a custom circular stopwatch.
 *
 * @author Oana Maria Teodorescu
 * @version 1.1
 */
public class CircularStopwatch extends CircleProgressBar {
    private ObjectAnimator objectAnimator;
    private ObjectAnimator blinkingAnimator;

    public CircularStopwatch(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Set the progress with an animation.
     * Note that the {@link android.animation.ObjectAnimator} Class automatically setField the progress
     * so don't call the {@link CircleProgressBar#setProgress(float)} directly within this method.
     *
     * @param progress       The progress it should animate to it.
     * @param duration       The duration in milliseconds for the animation.
     * @param listener       An Animator listener to track the animation.
     * @param updateListener An AnimatorUpdate listener to update the UI during the animation
     */
    public void setProgressWithAnimationAndListener(float progress, long duration,
                                                    Animator.AnimatorListener listener,
                                                    ValueAnimator.AnimatorUpdateListener updateListener) {

        objectAnimator = ObjectAnimator.ofFloat(this, "progress", progress);
        objectAnimator.setDuration(duration);
        objectAnimator.setInterpolator(new DecelerateInterpolator());
        objectAnimator.addListener(listener);
        objectAnimator.addUpdateListener(updateListener);
        objectAnimator.start();
    }

    /**
     * Check if animation is already running.
     *
     * @return {@code true} if animation is running, {@code false} otherwise
     */
    public boolean isAnimationRunning() {
        return objectAnimator != null && objectAnimator.isRunning();
    }

    /**
     * Cancel the animation, if already running.
     */
    public void cancelAnimation() {
        if (objectAnimator != null) {
            objectAnimator.removeAllListeners();
            objectAnimator.cancel();
        }
    }

    /**
     * Sets a blinking animation to the circular stopwatch.
     * <p>
     * <u>Note</u>: The repeat count must be an odd number for the UI element to be visible.
     *
     * @param duration      The duration of animation in milliseconds
     * @param repeatCount   The number of times to repeat
     */
    public void setBlinkingAnimation(long duration, int repeatCount) {
        blinkingAnimator = ObjectAnimator.ofInt(this, "color",
                getResources().getColor(R.color.coffee), Color.TRANSPARENT);
        blinkingAnimator.setDuration(duration);
        blinkingAnimator.setEvaluator(new ArgbEvaluator());
        blinkingAnimator.setRepeatCount(repeatCount);
        blinkingAnimator.setRepeatMode(ValueAnimator.REVERSE);
        blinkingAnimator.start();
    }

    /**
     * Cancel the blinking animation, if already running.
     */
    public void cancelBlink(int initialElementColor) {
        if (blinkingAnimator != null) {
            blinkingAnimator.cancel();
            //ReflectionUtility.setField(blinkingAnimator.getTarget(), blinkingAnimator.getPropertyName(), initialElementColor);
        }
    }
}
