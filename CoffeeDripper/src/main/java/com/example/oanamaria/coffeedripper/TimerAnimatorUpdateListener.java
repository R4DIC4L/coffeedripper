package com.example.oanamaria.coffeedripper;

import android.animation.ValueAnimator;
import android.widget.TextView;

/**
 * Class TimerAnimatorUpdateListener updates the animation UI timestamp during
 * the animation timer.
 *
 * @author Oana Maria Teodorescu
 * @version 1.2
 */
public class TimerAnimatorUpdateListener implements ValueAnimator.AnimatorUpdateListener {
    private long initialTime;
    private TextView txtTimerUpdate;

    public TimerAnimatorUpdateListener(long initialTime, TextView txtTimerUpdate) {
        this.initialTime = initialTime;
        this.txtTimerUpdate = txtTimerUpdate;
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        try {
            long millisec = initialTime + animation.getCurrentPlayTime();
            long sec = millisec / 1000;
            //millisec -= sec * 1000;
            long min = sec / 60;
            sec -= min * 60;
            txtTimerUpdate.setText(String.format("%02d:%02d", min, sec));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
